package com.example.digitalmediacorporate.mappers;

import com.example.digitalmediacorporate.DTOs.CharacterDTO;
import com.example.digitalmediacorporate.DTOs.FranchiseDTO;
import com.example.digitalmediacorporate.DTOs.FranchiseUpdateDTO;
import com.example.digitalmediacorporate.models.Character;
import com.example.digitalmediacorporate.models.Franchise;
import com.example.digitalmediacorporate.models.Movie;
import com.example.digitalmediacorporate.services.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {
    @Autowired
    protected MovieService movieService;

    /**
     * Creates a dto representing Franchise
     * @param franchise
     * @return
     */
    public abstract Collection<FranchiseDTO> franchiseToFranchiseDtos(Collection<Franchise> franchise);

    /**
     * Creates franchise representing franchiseDto
     * @param franchises
     * @return
     */
    public abstract Collection<FranchiseUpdateDTO> franchiseToFranchiseUpdateDtos(Collection<Franchise> franchises);


    /**
     * Takes a Franchise and turns into a dto
     * @param franchise
     * @return
     */
    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieToIds")
    public abstract FranchiseDTO franchiseToFranchiseDto(Franchise franchise);

    /**
     * Takes a dto and turns it into a franchise
     * @param dto
     * @return
     */
    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdsToMovies")
    public abstract Franchise franchiseDtoToFranchise(FranchiseDTO dto);

    /**
     * Takes dto and updates franchise database.
     * @param dto
     * @return
     */
    public abstract Franchise franchiseUpdateDtoToFranchise(FranchiseUpdateDTO dto);

    /**
     * maps movies and returns ids.
     * @param source
     * @return
     */
    @Named("movieToIds")
    Set<Long> map(Set<Movie> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }

    /**
     * maps ids and returns movies.
     * @param id
     * @return
     */
    @Named("movieIdsToMovies")
    Set<Movie> mapIdsToMovies(Set<Long> id) {
        return id.stream()
                .map( i -> movieService.findMovieById(i))
                .collect(Collectors.toSet());
    }
}
