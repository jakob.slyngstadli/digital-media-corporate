package com.example.digitalmediacorporate.mappers;

import com.example.digitalmediacorporate.DTOs.MovieDTO;
import com.example.digitalmediacorporate.models.Character;
import com.example.digitalmediacorporate.models.Franchise;
import com.example.digitalmediacorporate.models.Movie;
import com.example.digitalmediacorporate.services.CharacterService;
import com.example.digitalmediacorporate.services.FranciseService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {
    @Autowired
    protected FranciseService franciseService;
    @Autowired
    protected CharacterService characterService;

    /**
     * Creates a dto representing movie
     * @param movie
     * @return
     */
    @Mapping(target = "characters", source = "characters", qualifiedByName = "characterToIds")
    @Mapping(target = "franchise", source = "franchise.id")
    public abstract MovieDTO movieToMovieDto(Movie movie);

    /**
     * creates a collection of movieDtos
     * @param movie
     * @return
     */
    public abstract Collection<MovieDTO> movieToMovieDto(Collection<Movie> movie);

    /**
     * Takes a dto and turn it into a movie
     * @param dto
     * @return
     */
    @Mapping(target = "characters", source = "characters", qualifiedByName = "characterIdsToCharacters")
    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "franchiseToIds")
    public abstract Movie movieDtoToMovie(MovieDTO dto);

    /**
     * maps characters and returns ids
     * @param source
     * @return
     */
    @Named("characterToIds")
    Set<Long> map(Set<Character> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }

    /**
     * maps franchise and returns ids
     * @param id
     * @return
     */
    @Named("franchiseToIds")
    Franchise mapIdToFranchise(long id) {
        return franciseService.findFranchiseById(id);
    }

    /**
     * maps ids and returns characters
     * @param id
     * @return
     */
    @Named("characterIdsToCharacters")
    Set<Character> mapIdsToCharacter(Set<Long> id) {
        return id.stream()
                .map( i -> characterService.findCharacterById(i))
                .collect(Collectors.toSet());
    }

}
