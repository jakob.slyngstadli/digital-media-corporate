package com.example.digitalmediacorporate.mappers;

import com.example.digitalmediacorporate.DTOs.CharacterDTO;
import com.example.digitalmediacorporate.models.Character;
import com.example.digitalmediacorporate.models.Movie;
import com.example.digitalmediacorporate.services.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class CharacterMapper {
    @Autowired
    protected MovieService movieService;

    /**
     * Creates a Dto representing characters.
     * @param character
     * @return
     */
    public abstract Collection<CharacterDTO> charactersToCharacterDtos(Collection<Character> character);

    /**
     * Creates a Dto representing character.
     * @param character
     * @return
     */
    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieToIds")
    public abstract CharacterDTO characterToCharacterDto(Character character);

    /**
     * Takes a Dto and transform it to character.
     * @param dto
     * @return
     */
    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdsToMovies")
    public abstract Character characterDtoToCharacter(CharacterDTO dto);

    /**
     * Maps movies and returns ids.
     * @param source
     * @return
     */
    @Named("movieToIds")
    Set<Long> map(Set<Movie> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }

    /**
     * Maps ids and returns movies.
     * @param id
     * @return
     */
    @Named("movieIdsToMovies")
    Set<Movie> mapIdsToMovies(Set<Long> id) {
        return id.stream()
                .map( i -> movieService.findMovieById(i))
                .collect(Collectors.toSet());
    }
}
