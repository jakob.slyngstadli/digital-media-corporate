package com.example.digitalmediacorporate.controllers;

import com.example.digitalmediacorporate.DTOs.FranchiseUpdateMoviesDTO;
import com.example.digitalmediacorporate.DTOs.MovieDTO;
import com.example.digitalmediacorporate.DTOs.MovieUpdateCharactersDTO;
import com.example.digitalmediacorporate.mappers.MovieMapper;
import com.example.digitalmediacorporate.models.Franchise;
import com.example.digitalmediacorporate.models.Movie;
import com.example.digitalmediacorporate.services.MovieService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/movie")
public class MovieController {
    private final MovieService movieService;
    private final MovieMapper movieMapper;


    public MovieController(MovieService movieService, MovieMapper movieMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
    }

    public ResponseEntity getAllMovies () {
        List<Movie> movies = movieService.findAllMovies();
        return new ResponseEntity(movies, HttpStatus.OK);
    }

    @Operation(summary = "Get all movies")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Cant find movies",
                    content = @Content)
    })
    @GetMapping
    public ResponseEntity getAll() {
        Collection<MovieDTO> movies = movieMapper.movieToMovieDto(
                movieService.findAllMovies()
        );
        return ResponseEntity.ok(movies);
    }

    public ResponseEntity getMovieById (@PathVariable("id") long id) {
        Movie movie = movieService.findMovieById(id);
        return new ResponseEntity(movie, HttpStatus.OK);
    }

    @Operation(summary = "Find movie by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie does not exist with supplied ID",
                    content = @Content)
    })
    @GetMapping("{id}")
    public ResponseEntity getById(@PathVariable long id) {
        MovieDTO stud = movieMapper.movieToMovieDto(
                movieService.findMovieById(id)
        );
        return ResponseEntity.ok(stud);
    }

    @Operation(summary = "Add movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "400",
                    description = "Cant add movie to database",
                    content = @Content)
    })
    @PostMapping
    public ResponseEntity addMovie(@RequestBody Movie movie) {
        Movie newMovie = movieService.addMovie(movie);
        return new ResponseEntity(newMovie, HttpStatus.CREATED);
    }


    @Operation(summary = "Update movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = @Content)
    })
    @PutMapping
    public ResponseEntity updateMovie(@RequestBody Movie movie) {
        Movie updateMovie = movieService.updateMovie(movie);
        return new ResponseEntity(updateMovie, HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "Update characters in movies")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Movies not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("character/{id}")
    public ResponseEntity updateFranchiseMovies(@PathVariable long id, @RequestBody MovieUpdateCharactersDTO movieDTO) {
        Movie movie = movieService.updateMovieCharacters(id, movieDTO);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Delete movie by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie does not exist with supplied ID",
                    content = @Content)
    })
    @DeleteMapping("{id}")
    public ResponseEntity deleteMovie(@PathVariable("id") Long id) {
        movieService.deleteMovie(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
