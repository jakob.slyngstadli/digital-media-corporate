package com.example.digitalmediacorporate.controllers;

import com.example.digitalmediacorporate.DTOs.CharacterDTO;
import com.example.digitalmediacorporate.mappers.CharacterMapper;
import com.example.digitalmediacorporate.models.Character;
import com.example.digitalmediacorporate.models.Movie;
import com.example.digitalmediacorporate.services.CharacterService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/character")
public class CharacterController {

    private final CharacterService characterService;
    private final CharacterMapper characterMapper;

    public CharacterController(CharacterService characterService, CharacterMapper characterMapper) {
        this.characterService = characterService;
        this.characterMapper = characterMapper;
    }

    public ResponseEntity getAllCharacters () {
        List<Character> characters = characterService.findAllCharacters();
        return new ResponseEntity(characters, HttpStatus.OK);
    }

    @Operation(summary = "Get all Characters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Character.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Cant find characters",
                    content = @Content)
    })
    @GetMapping
    public ResponseEntity getAll() {
        Collection<CharacterDTO> characters = characterMapper.charactersToCharacterDtos(
                characterService.findAllCharacters()
        );
        return ResponseEntity.ok(characters);
    }

    public ResponseEntity getCharacterById (@PathVariable("id") Long id) {
        Character character = characterService.findCharacterById(id);
        return new ResponseEntity(character, HttpStatus.OK);
    }

    @Operation(summary = "Find character by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Character.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Character does not exist with supplied ID",
                    content = @Content)
    })
    @GetMapping("{id}")
    public ResponseEntity getById(@PathVariable long id) {
        CharacterDTO stud = characterMapper.characterToCharacterDto(characterService.findCharacterById(id));
        return ResponseEntity.ok(stud);
    }

    @Operation(summary = "Add character")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Character.class)) }),
            @ApiResponse(responseCode = "400",
                    description = "Cant add character to database",
                    content = @Content)
    })
    @PostMapping
    public ResponseEntity addCharacter(@RequestBody Character character) {
        Character newCharacter = characterService.addCharacter(character);
        return new ResponseEntity(newCharacter, HttpStatus.CREATED);
    }

    @Operation(summary = "Update character")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with supplied ID",
                    content = @Content)
    })
    @PutMapping
    public ResponseEntity updateCharacter(@RequestBody Character character) {
        Character updateCharacter = characterService.updateCharacter(character);
        return new ResponseEntity(updateCharacter, HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "Delete character by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Delete Character Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Character.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Character does not exist with supplied ID",
                    content = @Content)
    })
    @DeleteMapping("{id}")
    public ResponseEntity deleteCharacter(@PathVariable("id") Long id) {
        characterService.deleteCharacter(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
