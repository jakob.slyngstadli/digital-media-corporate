package com.example.digitalmediacorporate.controllers;

import com.example.digitalmediacorporate.DTOs.CharacterDTO;
import com.example.digitalmediacorporate.DTOs.FranchiseDTO;
import com.example.digitalmediacorporate.DTOs.FranchiseUpdateDTO;
import com.example.digitalmediacorporate.DTOs.FranchiseUpdateMoviesDTO;
import com.example.digitalmediacorporate.mappers.FranchiseMapper;
import com.example.digitalmediacorporate.models.Franchise;
import com.example.digitalmediacorporate.models.Movie;
import com.example.digitalmediacorporate.services.FranciseService;
import com.example.digitalmediacorporate.services.MovieService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/franchise")
public class FranchiseController {
    private final FranciseService franciseService;
    private final FranchiseMapper franchiseMapper;
    public FranchiseController(FranciseService franciseService, FranchiseMapper franchiseMapper) {
        this.franciseService = franciseService;
        this.franchiseMapper = franchiseMapper;
    }

    public ResponseEntity getAllMovies () {
        List<Franchise> franchises = franciseService.findAllFranchises();
        return new ResponseEntity(franchises, HttpStatus.OK);
    }

    @Operation(summary = "Get all franchises")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Cant find franchises",
                    content = @Content)
    })
    @GetMapping
    public ResponseEntity getAll() {
        Collection<FranchiseDTO> franchises = franchiseMapper.franchiseToFranchiseDtos(
                franciseService.findAllFranchises()
        );
        return ResponseEntity.ok(franchises);
    }

    public ResponseEntity getFranchiseById (@PathVariable("id") Long id) {
        Franchise franchise = franciseService.findFranchiseById(id);
        return new ResponseEntity(franchise, HttpStatus.OK);
    }

    @Operation(summary = "Find franchise by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = @Content)
    })
    @GetMapping("{id}")
    public ResponseEntity getById(@PathVariable long id) {
        FranchiseDTO fran = franchiseMapper.franchiseToFranchiseDto(franciseService.findFranchiseById(id));
        return ResponseEntity.ok(fran);
    }

    @Operation(summary = "Add franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class)) }),
            @ApiResponse(responseCode = "400",
                    description = "Cant add franchise to database",
                    content = @Content)
    })
    @PostMapping
    public ResponseEntity addFranchise(@RequestBody Franchise franchise) {
        Franchise newFranchise = franciseService.addFranchise(franchise);
        return new ResponseEntity(franchise, HttpStatus.CREATED);
    }

    @Operation(summary = "Update franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{id}")
    public ResponseEntity updateFranchise(@RequestBody FranchiseUpdateDTO franchiseDTO, @PathVariable long id) {
        if(franchiseDTO.getId() != id) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        Franchise tempFran = franchiseMapper.franchiseUpdateDtoToFranchise(franchiseDTO);
        Franchise updateFranchise = franciseService.updateFranchise(tempFran);
        return new ResponseEntity(updateFranchise, HttpStatus.NO_CONTENT); // TODO: 01.09.2022 Denen burde retunere positiv response 
    }

    @Operation(summary = "Update movies franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("movies/{id}")
    public ResponseEntity updateFranchiseMovies(@PathVariable long id, @RequestBody FranchiseUpdateMoviesDTO franchiseDTO) {
        Franchise franchise = franciseService.updateFranchiseMovies(id, franchiseDTO);
        return ResponseEntity.noContent().build();
    }
    @Operation(summary = "Delete franchise by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = @Content)
    })
    @DeleteMapping("{id}")
    public ResponseEntity deleteFranchise(@PathVariable("id") Long id) {
        franciseService.deleteFranchise(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
