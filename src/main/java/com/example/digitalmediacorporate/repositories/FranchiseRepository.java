package com.example.digitalmediacorporate.repositories;

import com.example.digitalmediacorporate.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Long> {

}
