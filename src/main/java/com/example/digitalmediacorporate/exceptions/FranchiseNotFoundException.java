package com.example.digitalmediacorporate.exceptions;

import com.example.digitalmediacorporate.models.Franchise;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class FranchiseNotFoundException extends RuntimeException{
    public FranchiseNotFoundException(String message) {
        super(message);
    }
}
