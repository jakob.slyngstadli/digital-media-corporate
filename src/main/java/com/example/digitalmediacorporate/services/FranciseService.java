package com.example.digitalmediacorporate.services;

import com.example.digitalmediacorporate.DTOs.FranchiseUpdateMoviesDTO;
import com.example.digitalmediacorporate.exceptions.FranchiseNotFoundException;
import com.example.digitalmediacorporate.mappers.FranchiseMapper;
import com.example.digitalmediacorporate.models.Franchise;
import com.example.digitalmediacorporate.models.Movie;
import com.example.digitalmediacorporate.repositories.FranchiseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class FranciseService {
    private final FranchiseRepository franchiseRepository;
    private final MovieService movieService;

    /**
     * Creates a franchise repository and a movieservice
     * @param franchiseRepository
     * @param movieService
     */
    @Autowired
    public FranciseService(FranchiseRepository franchiseRepository,MovieService movieService) {
        this.franchiseRepository = franchiseRepository;
        this.movieService = movieService;
    }

    /**
     * Allows you to add franchise
     * @param franchise
     * @return
     */
    public Franchise addFranchise(Franchise franchise) {
        return franchiseRepository.save(franchise);
    }

    /**
     * Returns all available franchises in database.
     * @return
     */
    public List<Franchise> findAllFranchises() {
        return franchiseRepository.findAll();
    }

    /**
     * Updates existing franchise
     * @param franchise
     * @return
     */
    public Franchise updateFranchise(Franchise franchise) {
        return franchiseRepository.save(franchise);
    }

    /**
     * Updates movie in a franchise.
     * @param id
     * @param franchiseDTO
     * @return
     */
    @Transactional
    public Franchise updateFranchiseMovies(long id, FranchiseUpdateMoviesDTO franchiseDTO) {
        Franchise franchise = findFranchiseById(id);
        Set<Movie> tempSet = findMoviesToUpdate(franchiseDTO);
        franchise.setMovies(tempSet);
        tempSet.forEach(m -> m.setFranchise(franchise));
        return updateFranchise(franchise);
    }

    /**
     * Creates a DTO representing franchise data.
     * @param franchiseDTO
     * @return
     */
    //Helper metode
    private Set<Movie> findMoviesToUpdate(FranchiseUpdateMoviesDTO franchiseDTO) {
        return franchiseDTO.getMovieIds().stream()
                .map(movieService::findMovieById)
                .collect(Collectors.toSet());
    }

    /**
     * Takes in id and returns corresponding franchise.
     * @param id
     * @return
     */
    public Franchise findFranchiseById(Long id) {
        return franchiseRepository.findById(id)
                .orElseThrow(() -> new FranchiseNotFoundException("Franchise by id " + id + " was not found"));
    }

    /**
     * Takes in id and deletes corresponding franchise.
     * @param id
     */
    public void deleteFranchise(Long id){
        franchiseRepository.deleteById(id);
    }

}

