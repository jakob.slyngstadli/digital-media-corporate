package com.example.digitalmediacorporate.services;

import com.example.digitalmediacorporate.exceptions.CharacterNotFoundException;
import com.example.digitalmediacorporate.models.Character;
import com.example.digitalmediacorporate.repositories.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CharacterService {
    private final CharacterRepository characterRepository;

    /**
     * Creates CharacterRepository
     * @param characterRepository
     */
    @Autowired
    public CharacterService(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    /**
     * Allows you to add characters.
     * @param character
     * @return
     */
    public Character addCharacter(Character character) {
        return characterRepository.save(character);
    }

    /**
     * @return
     */
    public List<Character> findAllCharacters() {
        return characterRepository.findAll();
    }

    /**
     * Updates existing characters with new info.
     * @param character
     * @return
     */
    public Character updateCharacter(Character character) {
        return characterRepository.save(character);
    }

    /**
     * Takes in characters id and returns correct character.
     * @param id
     * @return
     */
    public Character findCharacterById(Long id) {
        return characterRepository.findById(id)
                .orElseThrow(() -> new CharacterNotFoundException("Character by id " + id + " was not found"));
    }

    /**
     * Takes in id and deletes the character.
     * @param id
     */
    public void deleteCharacter(Long id){
        characterRepository.deleteById(id);
    }
}
