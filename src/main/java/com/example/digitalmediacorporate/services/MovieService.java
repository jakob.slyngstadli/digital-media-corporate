package com.example.digitalmediacorporate.services;

import com.example.digitalmediacorporate.DTOs.MovieUpdateCharactersDTO;
import com.example.digitalmediacorporate.exceptions.MovieNotFoundException;
import com.example.digitalmediacorporate.models.Character;
import com.example.digitalmediacorporate.models.Movie;
import com.example.digitalmediacorporate.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class MovieService {
    private final MovieRepository movieRepository;
    private final CharacterService characterService;

    /**
     * creates movie repository and character service
     * @param movieRepository
     * @param characterService
     */
    @Autowired
    public MovieService(MovieRepository movieRepository, CharacterService characterService) {
        this.movieRepository = movieRepository;
        this.characterService = characterService;
    }

    /**
     * Allows you to create new movie
     * @param movie
     * @return
     */
    public Movie addMovie(Movie movie) {return movieRepository.save(movie);
    }

    /**
     * Returns all available movies in database.
     * @return
     */
    public List<Movie> findAllMovies() {
        return movieRepository.findAll();
    }

    /**
     * Updates a existing movie-
     * @param movie
     * @return
     */
    public Movie updateMovie(Movie movie) {
        return movieRepository.save(movie);
    }

    /**
     * Takes in id and returns specific Movie.
     * @param id
     * @return
     */
    public Movie findMovieById(Long id) {
        return movieRepository.findById(id)
                .orElseThrow(() -> new MovieNotFoundException("Movie by id " + id + " was not found"));
    }

    /**
     * Takes in id and deletes specific movie.
     * @param id
     */
    public void deleteMovie(Long id){
        movieRepository.deleteById(id);
    }

    /**
     * Allows you to update the characters in a movie.
     * @param id
     * @param movieDTO
     * @return
     */
    @Transactional
    public Movie updateMovieCharacters(long id, MovieUpdateCharactersDTO movieDTO) {
        Set<Movie> movieSet = new HashSet<>();
        Movie tempMovie = findMovieById(id);
        movieSet.add(tempMovie);
        Set<Character> tempSet = findCharactersToUpdate(movieDTO);
        tempMovie.setCharacters(tempSet);
        tempSet.forEach(c -> c.setMovies(movieSet));
        return updateMovie(tempMovie);
    }

    /**
     * helper function for update character.
     * Checks if there is characters to update.
     * @param movieDTO
     * @return
     */
    private Set<Character> findCharactersToUpdate(MovieUpdateCharactersDTO movieDTO) {
        return movieDTO.getCharacterIds().stream()
                .map(characterService::findCharacterById)
                .collect(Collectors.toSet());

    }
}
