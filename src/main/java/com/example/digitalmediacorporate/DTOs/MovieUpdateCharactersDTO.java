package com.example.digitalmediacorporate.DTOs;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class MovieUpdateCharactersDTO {
    private Set<Long> characterIds;
}
