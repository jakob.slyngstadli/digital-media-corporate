package com.example.digitalmediacorporate.DTOs;

import com.example.digitalmediacorporate.models.Character;
import com.example.digitalmediacorporate.models.Franchise;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.net.URL;
import java.util.Set;

@Getter
@Setter
public class MovieDTO {
    private Long id;
    private String movieName;
    private String genre;
    private int releaseYear;
    private String movieDirector;
    private URL moviePicture;
    private URL movieTrailer;
    private Set<Long> characters;
    private long franchise;
}
