package com.example.digitalmediacorporate.DTOs;

import com.example.digitalmediacorporate.enums.Gender;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.net.URL;
import java.util.Set;

@Getter
@Setter
public class CharacterDTO {
    private Long id;
    private String fullName;
    private String alias;
    private Gender gender;
    private URL picture;
    private Set<Long> movies;
}
