package com.example.digitalmediacorporate.DTOs;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class FranchiseDTO {
    private Long id;
    private String franchiseName;
    private String description;
    private Set<Long> movies;
}
