package com.example.digitalmediacorporate.DTOs;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FranchiseUpdateDTO {
    private Long id;
    private String franchiseName;
    private String description;
}
