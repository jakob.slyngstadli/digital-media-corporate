package com.example.digitalmediacorporate.enums;

public enum Gender {
    MALE,
    FEMALE,
    NON_BINARY
}
