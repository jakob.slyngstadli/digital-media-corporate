package com.example.digitalmediacorporate.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.net.URL;
import java.util.Set;


@Entity
@Getter
@Setter
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    private Long id;

    @Column(length = 50, nullable = false)
    private String movieName;

    @Column(length = 100, nullable = false)
    private String genre;

    @Max(4)
    @Min(4)
    private int releaseYear;

    @Column(length = 50, nullable = false)
    private String movieDirector;

    @Column(length = 500, nullable = false)
    private URL moviePicture;

    @Column(length = 500, nullable = false)
    private URL movieTrailer;

    @ManyToMany(mappedBy = "movies")
    private Set<Character> characters;

    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;
}
