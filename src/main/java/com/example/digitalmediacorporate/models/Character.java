
package com.example.digitalmediacorporate.models;
import com.example.digitalmediacorporate.enums.Gender;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.net.URL;
import java.util.Set;


@Entity
@Getter
@Setter
public class Character{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "character_id")
    private Long id;
    @Column(length = 50, nullable = false)
    private String fullName;
    @Column(length = 50)
    private String alias;
    @NotNull
    @Enumerated(EnumType.STRING)
    private Gender gender;
    @Column(length = 500, nullable = false)
    private URL picture;
    @ManyToMany
    private Set<Movie> movies;
}
