package com.example.digitalmediacorporate.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "franchise_id")
    private Long id;

    @Column(length = 50, nullable = false)
    private String franchiseName;

    @Column(length = 500, nullable = false)
    private String description;

    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies;

}
