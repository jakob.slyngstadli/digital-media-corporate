INSERT INTO character (full_name,alias,gender,picture)
VALUES ('Mr man', 'The man', 'MALE', 'https://www.youtube.com/watch?v=i8ZW94SWchE'),
       ('Mr Woman', 'The woman', 'FEMALE', 'https://www.youtube.com/watch?v=i8ZW94SWchE'),
       ('Mr Binary', 'The Boulder', 'NON_BINARY', 'https://www.youtube.com/watch?v=i8ZW94SWchE');

INSERT INTO franchise(franchise_name,description)
VALUES ('Lord of the Wings: The Saga', 'Very nice eating'),
       ('Marvel', 'Big men with even bigger women'),
    ('Star Wars the Game', 'The stars and the wars collide');

INSERT INTO movie (movie_name,genre,movie_director,movie_picture,movie_trailer, release_year,franchise_id)
VALUES ('Star warts', 'Space, Drama, Action', 'Gearg Lucario', 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Star_Wars_Logo.svg/640px-Star_Wars_Logo.svg.png', 'https://www.youtube.com/watch?v=sGbxmsDFVnE', '1995',2),
       ('Lord of the Wings', 'Bingin Eating, Gore', 'Peter Jarkson', 'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcT9J7XACn3tlD6v4UXRMvT2wJN8FGCCPeh8U3RkZ6__tR4wGhSo', 'https://www.youtube.com/watch?v=uYnQDsaxHZU','2003',3),
       ('Mean Girls', 'Mean, Girls', 'Girl Meanie', 'https://upload.wikimedia.org/wikipedia/en/a/ac/Mean_Girls_film_poster.png','https://www.youtube.com/watch?v=uYnQDsaxHZU','2020',1);


