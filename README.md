# Not netflix
Not netflix is a royalty free API software that lets you, the owner, store and
manipulate a highly complex server. With cutting edge spring Data manipulation,
advanced hibernate and impeccable sql queries, you can't go wrong with this.

## Installation and 
- Install Java 17
- Install PGAdmin
- Download/Clone repository
- Open the API in an IDE such as IntelliJ

## How to use Not Netflix
- Run DigitalMediaCorporateApplication
- Open browser
- Paste in: http://localhost:8080/swagger-ui/index-html#
- Here you can run different functions

#### Special Achievements.
Solved the dreaded @Data bug.

## Authors
Co-created by Truls Ombye Hafnor and Jakob Slyngstadli
